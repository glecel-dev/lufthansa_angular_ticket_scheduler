# Lufthansa_angular_ticket_scheduler

# For Running the FrontEnd:
- $cd frontend
- npm i
- $ng serve

# For Running the Server in Postgresql:
- Create Database Lufthansa
 > If you have problems regarding the connection modify your database user and password in the files backend/lufthansa_backend/settings.py under "DB_USERNAME" and "DB_PASSWORD"

# For Running the Backend
- $cd backend
- $cd lufthansa-backend
- $source venv/bin/activate (venv/bin/activate--- for windows)
- pip install -r requirements.txt
- python manage.py migratetables
- python manage.py runserver

