export class Token {
  constructor(public access: string, public refresh: string, public username: string, public groups?: any, public permissions?: string[], public shops?: string[]) {
  }
}
