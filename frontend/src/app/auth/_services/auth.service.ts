import {Injectable} from '@angular/core';
import {Token} from '../_entities/auth.model';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpBackend, HttpClient} from '@angular/common/http';
import {FormBuilder} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {AUTH_ENDPOINTS} from '../../lufthansa-frontend/_common/endpoints';

export const CURRENT_USERNAME = 'username';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private readonly REFRESH_TOKEN = 'refresh';
  private readonly ACCESS_TOKEN = 'access';
  private readonly CURRENT_GROUPS = 'groups';
  private readonly CURRENT_PERMISSIONS = 'permissions';
  // export public CURRENT_USERNAME = 'username';
  private currentTokenSubject: BehaviorSubject<Token>;

  private currentToken: Observable<Token>;
  private httpWithoutInterceptor;
  LOGIN_URL = `rest-auth/google/`;

  today = new Date();
  tomorrow: Date = new Date();
  nextWeek: Date = new Date();

  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private httpBackEnd: HttpBackend,
    private cookieService: CookieService,
    private router: Router,
    private permissionsService: NgxPermissionsService,
    private rolesService: NgxRolesService
  ) {
    const access = this.cookieService.get(this.ACCESS_TOKEN);
    const refresh = this.cookieService.get(this.REFRESH_TOKEN);
    const username = this.cookieService.get(CURRENT_USERNAME);
    const groups = this.cookieService.get(this.CURRENT_GROUPS);

    this.tomorrow.setDate(this.today.getDate() + 1);
    this.nextWeek.setDate(this.today.getDate() + 7);

    this.currentTokenSubject = new BehaviorSubject<Token>({access, refresh, username, groups});
    this.currentToken = this.currentTokenSubject.asObservable();
    this.httpWithoutInterceptor = new HttpClient(httpBackEnd);
  }

  public getCurrentTokenValue(): any {
    return this.cookieService.getAll();
  }
  // @ts-ignore
  public login({username, password}) {
    return this.http.post<Token>(AUTH_ENDPOINTS.LOGIN_URL, {username, password}).pipe(
      map(token => {
        if (token) {
          console.log(token)
          this.setTokenToCookies(token);
          this.currentTokenSubject.next(token);
        }
      })
    );
  }

  async isLoggedInPermissions() {
    if (this.getCurrentTokenValue() != null) {
      await this.http.get(AUTH_ENDPOINTS.USER_PERMISSIONS_URL).toPromise().then((obj:any) => {
        this.cookieService.delete('permissions', '/');
        this.cookieService.set('permissions', JSON.stringify(obj['data']['permissions']), this.nextWeek, '/', '', false, 'Strict');

        this.permissionsService.loadPermissions(obj['data']['permissions']);
        // @ts-ignore

        for (const group of obj['data']['groups']) {
          this.rolesService.addRole(group, () => {
            return true;
          });
        }
      }).catch(err => {
        throw err;
      });
      return true;
    }
    await this.router.navigate(['/login']);
    return false;
  }
  setTokenToCookies(token: Token) {
    this.cookieService.set(this.CURRENT_GROUPS, token.groups, this.nextWeek, '/', '', false, 'Strict');
    this.cookieService.set(this.ACCESS_TOKEN, token.access, this.nextWeek, '/', '', false, 'Strict');
    this.cookieService.set(this.REFRESH_TOKEN, token.refresh, this.nextWeek, '/', '', false, 'Strict');
    this.cookieService.set(CURRENT_USERNAME, token.username, this.nextWeek, '/', '', false, 'Strict');
    console.log(token.permissions)
    this.cookieService.set(this.CURRENT_PERMISSIONS, JSON.stringify(token.permissions), this.nextWeek, '/', '', false, 'Strict');
    if(token.permissions){
      this.permissionsService.loadPermissions(token.permissions);
    }
    for (const group of this.CURRENT_GROUPS) {
      this.rolesService.addRole(group, () => {
        return true;
      });
    }
  }

  public logout() {
    this.cookieService.deleteAll('/');
    if (this.cookieService.check(this.CURRENT_GROUPS)) {
      this.cookieService.delete(this.CURRENT_GROUPS, '/');
    }
    // @ts-ignore

    this.currentTokenSubject.next(null);
    this.permissionsService.flushPermissions();
    this.rolesService.flushRoles();
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // return of(result as T);
      throw error;
    };
  }
}
