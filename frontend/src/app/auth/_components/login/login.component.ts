import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../_services/auth.service';
import {first} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {NgxPermissionsService} from 'ngx-permissions';
import {ADMINISTRATOR, USER_PERSON} from "../../../lufthansa-frontend/_common/cons";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // @ts-ignore

  loginForm: FormGroup;
  errors: any = {};
  // @ts-ignore

  private returnUrl: string;
  // @ts-ignore

  group;

  private readonly CURRENT_PERMISSIONS = 'permissions';
  title = 'Google Login';
  today = new Date();
  tomorrow: Date = new Date();
  nextWeek: Date = new Date();

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              private permissionsService: NgxPermissionsService,
              private snackBar: MatSnackBar,
              private cookieService: CookieService,
  ) {
  }

  ngOnInit(): void {
    this.authService.logout();
    this.cookieService.deleteAll();

    this.returnUrl = '';
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login() {
    this.hideErrors();
    const credentials = this.loginForm.value;
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.login(credentials)
      .pipe(first())
      .subscribe(response => {
        console.log(response)
          this.router.navigate([this.returnUrl]);
        },
        (error) => {
          this.loginForm.controls['username'].setErrors({'incorrect': true});
          this.loginForm.controls['password'].setErrors({'incorrect': true});
          this.snackBar.open(error.message, 'Mbyll', {duration: 3000});
        });
  }

  hideErrors() {
    this.loginForm.controls['username'].setErrors(null);
    this.loginForm.controls['password'].setErrors(null);
  }

  public getRole() {
    const a = this.cookieService.get('group');
    if (a === ADMINISTRATOR) {
      return ADMINISTRATOR;
    } else if (a === USER_PERSON) {
      return USER_PERSON;
    } else {
      return 'Anonymous';
    }
  }
}
