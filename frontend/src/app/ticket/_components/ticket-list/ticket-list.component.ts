import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import {CookieService} from 'ngx-cookie-service';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {Ticket} from "../../_entities/ticket.model";
import {AUTH_PERMISSIONS, SUPERUSER} from "../../../lufthansa-frontend/_common/cons";
import {DialogConfirmComponent} from "../../../lufthansa-frontend/_common/_components/dialog-confirm/dialog-confirm.component";
import {TicketService} from "../../_services/ticket.service";
import {merge} from "rxjs";
import { startWith, switchMap } from 'rxjs/operators';
import {TicketDetailsComponent} from "../ticket-details/ticket-details.component";
import { map, catchError } from 'rxjs/operators';

@UntilDestroy()

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css'],
  providers:[DialogConfirmComponent]
})
export class TicketListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'inbound', 'outbound', 'ticket_type_id', 'price', 'from_date', 'to_date', 'seat_number', 'update', 'delete'];
  dataList: Ticket[] | undefined;
  // @ts-ignore
  dataSource: MatTableDataSource<Ticket>;

  ACTIONS = {
    CAN_ADD: AUTH_PERMISSIONS.ADD_TICKET,
    CAN_CHANGE: AUTH_PERMISSIONS.CHANGE_TICKET,
    CAN_DELETE: AUTH_PERMISSIONS.DELETE_TICKET,
    CAN_DELETE_FIRST: SUPERUSER,
    CAN_ADD_FIRST: SUPERUSER,
    CAN_VIEW_FIRST: SUPERUSER,
  }
  first_ticket: any;
  createFirstSupply = false;
  pageSize = environment.PAGE_SIZE;
  resultsLength = 0;
  isLoadingResults = true;
  filteredListProgressBar: boolean | undefined;
  // @ts-ignore
  filterForm: FormGroup;
  callMeBack = this.ngOnInit;

  @ViewChild('paginatorTickets', {static: true}) paginator: MatPaginator | undefined;
  @ViewChild('sortTickets', {static: true}) sort: MatSort | undefined;

  constructor(
    private fb: FormBuilder,
    private ticketService: TicketService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private cookieService: CookieService,
    private deleteDialog: DialogConfirmComponent
  ) {
  }

  getFilterFormGroupValues() {
    return this.fb.group({
      ticket_type_id: ['', Validators.maxLength(10)],
      // is_first_ticket: [this.first_ticket],
      // shop: [JSON.parse(this.cookieService.get(USER_SHOP)), Validators.maxLength(10)],
    });
  }

  ngOnInit(): void {
    this.first_ticket = false;
    this.dataSource = new MatTableDataSource(this.dataList);
    this.filterForm = this.getFilterFormGroupValues();
    this.initializeSuppliesList();
  }

  initializeList() {
    this.dataSource = new MatTableDataSource(this.dataList);
    // console.log(this.checked);
    // @ts-ignore
    this.filterForm.controls['is_first_ticket'].patchValue(this.first_ticket);
    this.getSuppliesList();
  }

  initializeSuppliesList() {
    this.filteredListProgressBar = true;
    // @ts-ignore

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        untilDestroyed(this),
        startWith({}),
        switchMap(() => {
          return this.getSupplies();
        }),
        map((response: { pagination: { count: number; }; }) => {
          this.isLoadingResults = false;
          this.resultsLength = response.pagination.count;
          return response;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.filteredListProgressBar = false;
          return [];
        })
      ).subscribe(response => {
      // @ts-ignore
      this.dataSource = new MatTableDataSource<Ticket>(response.data);
      this.filteredListProgressBar = false;

    });
  }

  showDetails(row: { id: any; }) {
    const dt = Object.assign({}, row);
    const dialogRef = this.dialog.open(TicketDetailsComponent, {
      width: '1000px',
      panelClass: 'detail-component',
      data: row.id
    });
  }

  getSupplies() {

    this.isLoadingResults = true;
    // @ts-ignore
    return this.ticketService.getTickets(this.paginator, this.sort, this.filterForm.value);
  }

  private getSuppliesList() {
    this.getSupplies().subscribe((data: { pagination: { count: number; }; data: any[] | undefined; }) => {
      this.resultsLength = data.pagination.count;
      this.dataSource = new MatTableDataSource<Ticket>(data.data);
      this.isLoadingResults = false;
      console.log(data);
      this.createFirstSupply = this.first_ticket && this.dataSource.data.length <= 0;
      console.log(this.first_ticket);

    });
  }

  searchSupply() {
    this.getSuppliesList();
  }

  clearSupplyFilters() {
    // @ts-ignore
    this.filterForm.reset();
    this.first_ticket = '';
    // @ts-ignore
    // this.filterForm.controls['shop'].patchValue(JSON.parse(this.cookieService.get(USER_SHOP)));
  }

  addSupply() {
      this.router.navigateByUrl(`/ticket/form`);
  }

  updateSupply(id: any): void {
    this.router.navigateByUrl(`/ticket/form/${id}`);
  }

  deleteSupply(ticketId: any) {
    const dialogRef = this.deleteDialog.openDialog((value) => {
      if (value) {
          this.ticketService.deleteTicket(ticketId)
            .subscribe((data) => {
                this.snackBar.open('Operacioni përfundoi me sukses!', 'Mbyll', {duration: 2000});
                this.getSuppliesList();
              },
              (error) => {
                this.snackBar.open(error.message, 'Mbyll', {duration: 3000});
              });


      }
    });
  }
}
