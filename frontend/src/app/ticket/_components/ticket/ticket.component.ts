import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {TicketService} from "../../_services/ticket.service";
import {Location} from '@angular/common';
import {DateFormat, scFormatDateToStringPostgres, scFormatStringToDate} from '../../../lufthansa-frontend/_common/cons';
import {DateAdapter} from '@angular/material/core';
import {TICKET_LAST_URL} from "../../../lufthansa-frontend/_common/endpoints";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
  providers:[{provide: DateAdapter, useClass: DateFormat}]
})
export class TicketComponent implements OnInit {
  // @ts-ignore
  ticketForm: FormGroup;
  currencies: any;
  loading = false;
  errors: any = {};
  last_id:any;
  constructor(
    protected http: HttpClient,
    private fb: FormBuilder,
    private route: ActivatedRoute,

    private snackBar: MatSnackBar,
    private router: Router,
    private ticketService: TicketService,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this.ticketForm = this.fb.group({
      inbound: ['', Validators.required],
      outbound: ['', Validators.required],
      price: ['', Validators.required],
      ticket_type: ['', Validators.required],
      ticket_type_id: ['',],
      from_date: [new Date(), Validators.required],
      to_date: [new Date(), Validators.required],
      seat_number: ['', Validators.required],
    });
  this.http.get(TICKET_LAST_URL).subscribe(
    response =>{
      // console.log(response)
      this.last_id = response
    }
  )
    // @ts-ignore
    const id = +this.route.snapshot.paramMap.get('id');
    if (id) {
      this.ticketService.getTicket(id).subscribe(response=>{
        this.ticketForm.patchValue(response['data'])
      })
    }
    }


  // @ts-ignore
  saveTicket() {
    if (this.ticketForm.valid) {
      const formValues = Object.assign({}, this.ticketForm.value);
      this.ticketForm.controls.ticket_type_id.setValue(this.last_id+this.ticketForm.controls.ticket_type.value)
        return this.ticketService.postTicket(formValues).subscribe(
          response => {
            this.snackBar.open('Bileta u ruajt me sukses', 'Mbyll', {duration: 3000});

          }, error => {
            this.manageErrors(error);
          }
        );

    }
  }


  // @ts-ignore
  manageErrors(error) {
    const errorFields = ['inbound', 'outbound', 'ticket_type_id', 'price', 'from_date', 'to_date', 'seat_number'];
    const result = Object.keys(error.errors).filter(item => errorFields.some((otherItem) => item === otherItem));
    for (const key of result) {
      this.ticketForm.controls[key].setErrors({'server_error': error.errors[key][0].message});
    }

    if ('non_field_errors' in error.errors) {
      this.snackBar.open(error.errors['non_field_errors'][0].message, 'Mbyll', {duration: 5000});
    }
  }

  goBack() {
    this.location.back();
  }

}
