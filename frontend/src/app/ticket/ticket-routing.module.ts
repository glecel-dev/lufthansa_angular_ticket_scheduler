import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AUTH_PERMISSIONS, MESSAGE_403, SUPERUSER} from '../lufthansa-frontend/_common/cons';
import {TicketComponent} from "./_components/ticket/ticket.component";
import {TicketListComponent} from "./_components/ticket-list/ticket-list.component";


const routes: Routes = [
  {
    path: 'list', component: TicketListComponent, data: {
      permissions: {
        only: AUTH_PERMISSIONS.VIEW_TICKET, SUPERUSER,
        redirectTo: {
          navigationCommands: ['error-page'],
          navigationExtras: {
            state: {
              message: MESSAGE_403,
            }
          }
        },
      }
    }
  },
  {
    path: 'form', component: TicketComponent, data: {
      permissions: {
        only: AUTH_PERMISSIONS.ADD_TICKET,
        redirectTo: {
          navigationCommands: ['error-page'],
          navigationExtras: {
            state: {
              message: MESSAGE_403,
            }
          }
        },
      }
    }
  },
  {
    path: 'form/:id', component: TicketComponent, data: {
      permissions: {
        only: AUTH_PERMISSIONS.CHANGE_TICKET,
        redirectTo: {
          navigationCommands: ['error-page'],
          navigationExtras: {
            state: {
              message: MESSAGE_403,
            }
          }
        },
      }
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TicketRoutingModule {
}
