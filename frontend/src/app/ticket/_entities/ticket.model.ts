import {Filter} from "../../lufthansa-frontend/_common/_services/lufthansa-frontend.service";

export class Ticket {
  constructor(public id: number,
              public inbound: string,
              public outbound: number,
              public ticket_type: string,
              public ticket_type_id: string,
              public from_date: string,
              public to_date: string,
              public seat_number: string,
  ) {
  }
}

export class TicketFilter implements Filter {
  constructor(public supplier: string) {
  }

  getSupplierName() {
    return this.supplier ? this.supplier : '';
  }


  serialize(filterPrefix: string): string {
    return `${filterPrefix}_ticket__first_name=${this.getSupplierName()}`;
  }
}
