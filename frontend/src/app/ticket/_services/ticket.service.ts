import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {
  LufthansaFrontendEntityResponse,
  LufthansaFrontendListResponse,
  LufthansaFrontendService
} from "../../lufthansa-frontend/_common/_services/lufthansa-frontend.service";
import {TICKET_URL} from "../../lufthansa-frontend/_common/endpoints";
import {Observable} from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Ticket} from "../_entities/ticket.model";

@Injectable({
  providedIn: 'root'
})
export class TicketService extends LufthansaFrontendService {

  constructor(protected http: HttpClient) {
    super(http, TICKET_URL);
  }


  getTicket(id: number): Observable<LufthansaFrontendEntityResponse<Ticket>> {
    return this.getObject(id);
  }

  getTickets(page: MatPaginator | undefined, sort: MatSort | undefined, filter: any): Observable<LufthansaFrontendListResponse<Ticket>> {
    return this.getObjects(page, sort, filter);
  }


  postTicket(ticket: Ticket): Observable<LufthansaFrontendEntityResponse<Ticket>> {
    return this.postObject(ticket);
  }

  putTicket(ticket: Ticket): Observable<LufthansaFrontendEntityResponse<Ticket>> {
    return this.putObject(ticket);
  }

  deleteTicket(id: number): Observable<any> {
    return this.deleteObject(id);
  }
}
