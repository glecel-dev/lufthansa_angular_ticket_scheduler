import {NativeDateAdapter} from '@angular/material/core';
import {Injectable} from '@angular/core';

@Injectable()
export class DateFormat extends NativeDateAdapter {
  useUtcForDisplay = true;

  parse(value: any): Date | null {
    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');
      const year = Number(str[2]);
      const month = Number(str[1]) - 1;
      const date = Number(str[0]);
      return new Date(year, month, date);
    }
    const timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }
}

export function scFormatStringToDate(dateStrInput: { toString: () => any; }): Date | null {
  try {
    const dateStr = dateStrInput.toString();
    const dateParts = dateStr.split('-');
    const date = new Date(+dateParts[0], +dateParts[1] - 1, +dateParts[2]);
    return (date instanceof Date && !isNaN(date.getTime())) ? date : null;
  } catch (e) {
    return null;
  }
}

export function scFormatStringToDateTime(dateTimeStrInput: { toString: () => any; }): Date | null {
  try {
    const dateTimeStr = dateTimeStrInput.toString();
    const dateTimeParts = dateTimeStr.split(' ');
    const datePart = dateTimeParts[0];
    const timePart = dateTimeParts[1];
    const dateParts = datePart.split('-');
    const timeParts = timePart.split(':');
    const date = new Date(+dateParts[0], +dateParts[1] - 1, +dateParts[2], +timeParts[0], +timeParts[1]);
    return (date instanceof Date && !isNaN(date.getTime())) ? date : null;
  } catch (e) {
    return null;
  }
}

export function scFormatDateToString(dateInput: any): string | '' {
  try {
    const date = dateInput;
    return (date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear());
  } catch (e) {
    return '';
  }
}

export function scFormatDateToStringPostgres(dateInput: any): string | "" | null {
  try {
    const date = dateInput;
    return (date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate());
  } catch (e) {
    return null;
  }

}

export const today = new Date();
export const tomorrow: Date = new Date(today.getDate() + 1);
export const nextWeek: Date = new Date(today.getDate() + 7);

export const ADMINISTRATOR = 'Admin';
export const USER_PERSON = 'User';



export const MESSAGE_403 = 'JU NUK KENI TË DREJTA TË MJAFTUESHME';
export const SUPERUSER = 'SUPERUSER';


export enum AUTH_PERMISSIONS {
  VIEW_TICKET = 'ticket.view_ticket',
  ADD_TICKET = 'ticket.add_ticket',
  CHANGE_TICKET = 'ticket.change_ticket',
  DELETE_TICKET = 'ticket.delete_ticket',

}
