import {environment} from '../../../environments/environment';


export const TICKET_URL = `ticket/`;
export const TICKET_LAST_URL = `${environment.API_HOST}/ticket/last-id/`;


export const LOGIN_URL = `${environment.API_HOST}/auth/knock-knock/`;
export const GET_USER_SHOPS_URL = `${environment.API_HOST}/auth/user-shops/`;
export const USER_PERMISSIONS_URL = `${environment.API_HOST}/auth/user-permissions/`;
export const CHANGE_PASSWORD_URL = `${environment.API_HOST}/auth/change-password/`;



export const AUTH_ENDPOINTS = {
  LOGIN_URL,
  GET_USER_SHOPS_URL,
  USER_PERMISSIONS_URL,
  CHANGE_PASSWORD_URL
};



