import {Component, Input, OnInit} from '@angular/core';
import {LoaderService} from '../../_services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  typeBar: boolean | undefined;

  constructor(public loaderService: LoaderService) {
  }

  @Input() type: any;

  ngOnInit() {
    if (this.type || this.type === 'progressBar') {
      this.typeBar = false;
    } else {
      this.loaderService.destroyLoader();
    }
  }

  show() {
    if (!this.typeBar) {
      this.loaderService.buildLoader();
    } else {
      this.typeBar = true;
    }
  }

  hide() {
    if (!this.typeBar) {
      this.loaderService.destroyLoader();
    } else {
      this.typeBar = false;
    }
  }

  setText(value: string | undefined) {
    if (!this.typeBar) {
      this.loaderService.getText(value);
    }
  }

}
