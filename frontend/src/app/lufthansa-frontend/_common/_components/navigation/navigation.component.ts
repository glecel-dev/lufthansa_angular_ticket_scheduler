import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {AuthService, CURRENT_USERNAME} from "../../../../auth/_services/auth.service";
import {CookieService} from "ngx-cookie-service";
import {NgxRolesService} from "ngx-permissions";
import {FormControl} from "@angular/forms";
import {AUTH_PERMISSIONS, SUPERUSER} from "../../cons";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {


  today = new Date();
  nextWeek: Date = new Date();

  constructor(public route: Router,
              public dialog: MatDialog,
              private authService: AuthService,
              private cookieService: CookieService,
              private rolesService: NgxRolesService) {
    this.nextWeek.setDate(this.today.getDate() + 7);
  }


  username: string | undefined;
  currentComponent: any;
  hasTitle: boolean | undefined;
  title: string | undefined;


  TICKET_URLS = [{routerLink: '/ticket/list/', name: 'Lista Bileve', permission_name: [AUTH_PERMISSIONS.VIEW_TICKET, SUPERUSER]},
  ];
  // CONFIGURATIONS_URLS = [
  //   {routerLink: `/shop-default/list`, name: 'Konfigurimet e Dyqanit', permission_name: [AUTH_PERMISSIONS.VIEW_SHOP_DEFAULT]},
  //   {routerLink: '/permissions/permission-form/', name: 'Autorizimet', permission_name: [AUTH_PERMISSIONS.VIEW_PERMISSIONS]},
  //   {routerLink: '/currency/list/', name: 'Monedha', permission_name: [AUTH_PERMISSIONS.VIEW_CURRENCY]},
  //   {routerLink: `/offer/list`, name: 'Regjistri i Ofertave', permission_name: [AUTH_PERMISSIONS.VIEW_OFFER]},
  //
  // ];
  // SALES_URLS = [
  //   {routerLink: '/sale/list/', name: 'Regjistri i Shitjeve', permission_name: [AUTH_PERMISSIONS.VIEW_SALE]},
  //   {routerLink: '/sale-payment/list/', name: 'Regjistri i Arkëtimeve', permission_name: [AUTH_PERMISSIONS.VIEW_SALE_PAYMENT]},
  //   {routerLink: '/order/list/', name: 'Regjistri i Porosive', permission_name: [AUTH_PERMISSIONS.VIEW_ORDER]},
  //   {routerLink: '/customer/list/', name: 'Regjistri i Klientëve', permission_name: [AUTH_PERMISSIONS.VIEW_CUSTOMER]},
  //
  // ];
  // SUPPLIES_URLS = [
  //   {routerLink: '/supplier/list/', name: 'Regjistri i Furnitorëve', permission_name: [AUTH_PERMISSIONS.VIEW_SUPPLIER]},
  //   {routerLink: '/supply/list/', name: 'Regjistri i Furnizimeve', permission_name: [AUTH_PERMISSIONS.VIEW_SUPPLY, SUPERUSER]},
  //   {routerLink: '/supply-payment/list/', name: 'Regjistri i Pagesave', permission_name: [AUTH_PERMISSIONS.VIEW_SUPPLY_PAYMENT]},
  // ];
  // FINANCIALS_URLS = [
  //   {routerLink: `/financial-account/list`, name: 'Llogaritë Financiare', permission_name: [AUTH_PERMISSIONS.VIEW_FINANCIAL_ACCOUNT]},
  //
  // ];
  // WHAREHOUSE_URLS = [
  //   {routerLink: '/warehouse/product-stock/list/', name: 'Magazina', permission_name: [AUTH_PERMISSIONS.VIEW_PRODUCT_STOCK]},
  //
  // ];
  // PRODUCTS_URLS = [
  //   {routerLink: '/product/list/', name: 'Regjistri i Produkteve', permission_name: [AUTH_PERMISSIONS.VIEW_PRODUCT]},
  //   {routerLink: '/brand/list/', name: 'Marka', permission_name: [AUTH_PERMISSIONS.VIEW_BRAND]},
  //   {routerLink: '/category/list/', name: 'Kategori', permission_name: [AUTH_PERMISSIONS.VIEW_PRODUCT_CATEGORY]},
  // ];
  // REPORTS_URL = [];


  @ViewChild('drawer', {static: true}) drawer: any;

  ngOnInit() {
    console.log(this.cookieService)
    this.username = this.cookieService.get(CURRENT_USERNAME);
    this.hasTitle = false;
    this.title = 'Lufthansa';
    this.changeHoverEffect();

  }


  @HostListener('document:click')
  clickout() {
    const subs = Array.from(document.querySelectorAll('.sub'));
    for (const sub of subs) {
      // @ts-ignore
      sub['style'].opacity = 0;
    }
  }

  changeHoverEffect() {

    const name = Array.from(document.querySelectorAll('.has-sub-menu'));
    const no_sub_menu = Array.from(document.querySelectorAll('.no-sub-menu'));
    const subs = Array.from(document.querySelectorAll('.sub'));
    for (const no_sub of no_sub_menu) {
      no_sub.addEventListener('mouseover', () => {
        for (const sub of subs) {
          // @ts-ignore
          sub['style'].opacity = 0;
          // @ts-ignore

          sub['style'].pointerEvents = 'none';
        }
      });
    }
    for (let i = 0; i < name.length; i++) {
      name[i].addEventListener('mouseover', () => {
        for (let j = 0; j < subs.length; j++) {
          if (i !== j) {
            // @ts-ignore

            subs[j]['style'].opacity = 0;
            // @ts-ignore

            subs[j]['style'].pointerEvents = 'none';
          } else {
            // @ts-ignore

            subs[j]['style'].opacity = 1;
            // @ts-ignore

            subs[j]['style'].pointerEvents = 'auto';
          }
        }
      });

    }

  }


  logout() {
    this.route.navigate(['login/']);
  }
}
