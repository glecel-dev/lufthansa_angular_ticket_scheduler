import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss']
})
export class DialogConfirmComponent implements OnInit {
  public message!: string;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    this.message = 'Jeni të sigurt për këtë veprim?';

  }
  openDialog = (callback: { (value: any): void; (arg0: any): void; }) => {
    const currentDialog = this.dialog.open(DialogConfirmComponent, {width: '300px'});
    currentDialog.afterClosed().subscribe(result => {
      callback(result);
      this.message = '';
    });
  };
}
