import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-dialog-confirm',
  templateUrl: './custom-dialog-confirm.component.html',
  styleUrls: ['./custom-dialog-confirm.component.css']
})
export class CustomDialogConfirmComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
