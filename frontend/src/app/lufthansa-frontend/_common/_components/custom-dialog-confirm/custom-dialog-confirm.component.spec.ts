import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomDialogConfirmComponent } from './custom-dialog-confirm.component';

describe('CustomDialogConfirmComponent', () => {
  let component: CustomDialogConfirmComponent;
  let fixture: ComponentFixture<CustomDialogConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomDialogConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomDialogConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
