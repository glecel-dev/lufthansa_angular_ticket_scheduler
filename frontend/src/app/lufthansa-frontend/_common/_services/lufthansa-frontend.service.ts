import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {formatDate} from '@angular/common';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {T} from "@angular/cdk/keycodes";

export const MIN_TIME = '00:00:00.000000';
export const MAX_TIME = '23:59:59.999999';

export class LufthansaFrontendService {

  private static readonly FILTER_PREFIX = 'flt';
  protected readonly API_ENDPOINT:any;

  constructor(protected http: HttpClient, private apiURL: string) {
    this.API_ENDPOINT = LufthansaFrontendService.buildAPIEndpoint(environment.API_HOST, apiURL);
  }

  static buildQueryString(paginator: MatPaginator, sort: MatSort, filter: any): string {
    const queryString = [];
    const page_size: number = paginator.pageSize ? paginator.pageSize : 10;
    let page: number = paginator.pageIndex ? paginator.pageIndex : 0;
    page++;  // mat-paginator starts indexing from zero
    if (sort) {
      const ordering = sort ? getSort(sort.active, sort.direction) : '-id';
      queryString.push(`ordering=${ordering}`);
    }
    if (page) {
      queryString.push(`page=${page}`);
    }
    if (filter) {
      queryString.push(buildFilterString(filter));
    }
    if (queryString.length > 0) {
      return '?' + queryString.join('&');
    }
    return '';
  }

  private static buildAPIEndpoint(apiHost: string, apiURL: string) {
    return `${apiHost}/${apiURL}`;
  }

  getObjects<T>(paginator: MatPaginator | undefined, sort: MatSort | undefined, filter: any): Observable<LufthansaFrontendListResponse<T>> {
    // page = page ? page + 1 : 0;
    // page = page ? page += 1 : null;
    // @ts-ignore

    return this.http.get<LufthansaFrontendListResponse<T>>(this.API_ENDPOINT + LufthansaFrontendService.buildQueryString(paginator, sort, filter)).pipe(
      catchError(this.handleError(`getObjects`, null))
    );
  }

  getObject<T>(objectId: number): Observable<LufthansaFrontendEntityResponse<T>> {

    return this.http.get<LufthansaFrontendEntityResponse<T>>(this.API_ENDPOINT + objectId + '/');
  }

  postObject<T>(object: T): Observable<LufthansaFrontendEntityResponse<T>> {

    return this.http.post<LufthansaFrontendEntityResponse<T>>(this.API_ENDPOINT, object);
  }

  putObject<T>(object: T): Observable<LufthansaFrontendEntityResponse<T>> {

    // @ts-ignore
    return this.http.put<LufthansaFrontendEntityResponse<T>>(this.API_ENDPOINT + `${object['id']}/`, object);
  }

  deleteObject<T>(objectId: number): Observable<LufthansaFrontendEntityResponse<T>> {
    return this.http.delete<LufthansaFrontendEntityResponse<T>>(this.API_ENDPOINT + `${objectId}/`);
  }


  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      throw error;
    };
  }
  // @ts-ignore

  protected putObjectFiles(data, objectId) {
    return this.http.put(this.API_ENDPOINT + `${objectId}/`, data);
  }
  // @ts-ignore

  protected putObjectData(data, objectId) {
    return this.http.put(this.API_ENDPOINT + `${objectId}/`, data);
  }
  // @ts-ignore

  protected postObjectRequest(path, data) {
    return this.http.post(this.API_ENDPOINT + path, data);
  }

}

function buildFilterString(_filter: { [key: string]: any }) {
  const filter = {..._filter};
  let filterString = '';
  Object.keys(filter).forEach((key) => {
    let value = filter[key];
    if (value !== null) {
      if (value instanceof Date) {
        value = reformatDate(key, value);
      }
      filterString += `${key}=${value}&`;
    }
  });
  return filterString;
}

function reformatDate(field_name: string, date: Date): string {
  if (field_name.includes('_before')) {
    return formatDateToString(date, MAX_TIME);
  } else if (field_name.includes('_after')) {
    return formatDateToString(date, MIN_TIME);
  } else {
    return formatDateToString(date);
  }
}

export function formatDateToString(date: Date | string, time: string = ''): string {
  if (typeof date === 'object') {
    let formattedDate: string = formatDate(date, 'yyyy-MM-dd', 'en-us');
    if (time) {
      formattedDate += `+${time}`;
    }
    return formattedDate;
  }
  return date;
}

export interface Filter {
  serialize(filterPrefix: string): string;
}

export function getSort(active: string, direction: 'asc' | 'desc' | '') {
  const sort = direction === 'desc' ? '-' : '';
  return sort + active;
}

export interface LufthansaFrontendListResponse<T> {
  data: T[];
  pagination: { count: number; previous?: string; next?: string };
}

export interface LufthansaFrontendEntityResponse<T> {
  data: T;
}
