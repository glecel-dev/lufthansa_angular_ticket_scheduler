import { TestBed } from '@angular/core/testing';

import { LufthansaFrontendService } from './lufthansa-frontend.service';

describe('LufthansaBackendService', () => {
  let service: LufthansaFrontendService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LufthansaFrontendService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
