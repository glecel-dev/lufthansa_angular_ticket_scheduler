import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public visible: boolean | undefined;
  public text: string | undefined;

  constructor() {
  }

  destroyLoader() {
    this.visible = false;
    this.text = '';
  }

  buildLoader() {
    this.visible = true;
  }

  getText(value: string | undefined) {
    this.text = value;
  }

}
