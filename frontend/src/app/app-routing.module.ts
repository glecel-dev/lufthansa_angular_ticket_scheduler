import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NavigationComponent} from './lufthansa-frontend/_common/_components/navigation/navigation.component';
import {ErrorPageComponent} from './lufthansa-frontend/_common/_components/error-page/error-page.component';
import {NgxPermissionsGuard} from 'ngx-permissions';
import {AUTH_PERMISSIONS, MESSAGE_403} from './lufthansa-frontend/_common/cons';
import {AuthGuard} from "./auth/auth.guard";

const routes: Routes = [
  {path: 'login', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)},
  {path: 'error-page', component: ErrorPageComponent},
  {
    path: '', component: NavigationComponent, children: [
      {
        path: 'ticket',
        loadChildren: () => import('./ticket/ticket.module').then(m => m.TicketModule),
        canActivate: [NgxPermissionsGuard],
        canActivateChild: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: AUTH_PERMISSIONS.VIEW_TICKET,
            redirectTo: {
              navigationCommands: ['error-page'],
              navigationExtras: {
                state: {
                  message: MESSAGE_403,
                }
              }
            },
          }
        }
      },],
    canLoad: [AuthGuard],
    canActivate: [AuthGuard]
  },


  {path: '**', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
