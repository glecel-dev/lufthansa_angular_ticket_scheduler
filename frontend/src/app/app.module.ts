import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {ConfirmationDialogComponent} from './lufthansa-frontend/_common/_components/confirmation-dialog/confirmation-dialog.component';
import {CustomDialogConfirmComponent} from './lufthansa-frontend/_common/_components/custom-dialog-confirm/custom-dialog-confirm.component';
import {DialogConfirmComponent} from './lufthansa-frontend/_common/_components/dialog-confirm/dialog-confirm.component';
import {ErrorPageComponent} from './lufthansa-frontend/_common/_components/error-page/error-page.component';
import {LoaderComponent} from './lufthansa-frontend/_common/_components/loader/loader.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {AuthInterceptor} from "./lufthansa-frontend/_common/_interceptors/auth.interceptor";
import {ErrorInterceptor} from "./lufthansa-frontend/_common/_interceptors/error.interceptor";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatDialogModule} from "@angular/material/dialog";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatNativeDateModule} from "@angular/material/core";
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxPermissionsModule} from 'ngx-permissions';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from "./app-routing.module";
import {AuthGuard} from "./auth/auth.guard";
import {NavigationComponent} from './lufthansa-frontend/_common/_components/navigation/navigation.component';
import {RouterModule} from "@angular/router";

const MaterialModules = [
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatToolbarModule,
  MatButtonModule,
  MatCardModule,
  MatSidenavModule,
  MatListModule,
  MatDividerModule,
  MatIconModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatProgressBarModule,
  FlexLayoutModule,
  MatNativeDateModule,
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DialogConfirmComponent,
    LoaderComponent,
    CustomDialogConfirmComponent,
    ErrorPageComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    NgxPermissionsModule.forRoot(),
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModules,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule
  ],
  entryComponents: [ConfirmationDialogComponent, DialogConfirmComponent, CustomDialogConfirmComponent],

  providers: [
    HttpClient,
    HttpClientModule,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
