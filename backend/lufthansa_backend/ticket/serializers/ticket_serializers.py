from rest_framework.serializers import ModelSerializer

from ticket.models import Ticket


class TicketReadSerializer(ModelSerializer):
    class Meta:
        model = Ticket
        fields = ['id', 'inbound', 'outbound', 'ticket_type', 'ticket_type_id', 'price', 'from_date', 'to_date',
                  'seat_number']
        extra_kwargs = {'id': {'read_only': True}}


class TicketWriteSerializer(ModelSerializer):
    class Meta:
        model = Ticket
        fields = ['inbound', 'outbound', 'ticket_type', 'ticket_type_id', 'price', 'from_date', 'to_date',
                  'seat_number']
        extra_kwargs = {'id': {'read_only': True}}
