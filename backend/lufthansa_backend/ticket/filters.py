from django_filters import rest_framework as filters

from ticket.models import Ticket


class TicketFilterSerializer(filters.FilterSet):
    ticket_type_id = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Ticket
        fields = ['ticket_type_id']

    @property
    def qs(self):
        request = self.request
        if request is None:
            return Ticket.objects.none()

        parent = super(TicketFilterSerializer, self).qs
        return parent
