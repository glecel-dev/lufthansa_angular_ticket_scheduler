from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from lufthansa_backend.cons import DATA
from ticket.models import Ticket
from ticket.serializers.ticket_serializers import TicketReadSerializer, TicketWriteSerializer
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.permissions import AllowAny
from lufthansa_backend.common.api_views import MCListCreateAPIView, MCRetrieveUpdateDestroyAPIView
from ticket.filters import TicketFilterSerializer

class TicketListCreate(MCListCreateAPIView):
    queryset = Ticket.objects.all()
    read_serializer_class = TicketReadSerializer
    write_serializer_class = TicketWriteSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = TicketFilterSerializer
    ordering_fields = ['id', 'name', 'logo', 'website']


class TicketGetPutDelete(MCRetrieveUpdateDestroyAPIView):
    class_name = Ticket
    queryset = Ticket.objects.all()
    read_serializer_class = TicketReadSerializer
    write_serializer_class = TicketWriteSerializer


@api_view(['GET'])
@permission_classes([AllowAny])
def get_last_id(request):
    latest_id = Ticket.objects.order_by('id')
    if len(latest_id) > 0:
        return Response(Ticket.objects.order_by('id')[0].id)
    else:
        return Response(0)
