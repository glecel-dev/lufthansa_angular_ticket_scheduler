from django.urls import path

from ticket.model_views.ticket_views import TicketListCreate, TicketGetPutDelete, get_last_id

urlpatterns = [
    path('ticket/', TicketListCreate.as_view(), name='tickets'),
    path('ticket/<int:pk>/', TicketGetPutDelete.as_view(), name='ticket'),
    path('ticket/last-id/', get_last_id, name='ticket-last-id'),
]
