from django.db import models
from django_common.auth_backends import User


class LBModel(models.Model):
    class Meta:
        app_label = 'Ticket'
        abstract = True


class Ticket(LBModel):
    class Meta:
        verbose_name = 'Ticket'
        verbose_name_plural = 'Tickets'
        db_table = 'lb_ticket'
        ordering = ['-id']
        unique_together = ('inbound', 'outbound', 'ticket_type', 'ticket_type_id', 'price', 'from_date', 'to_date',
                           'seat_number')

    inbound = models.CharField(max_length=150)
    outbound = models.CharField(max_length=150)
    ticket_type = models.CharField(max_length=50)
    ticket_type_id = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=26, decimal_places=3, max_length=50, blank=True, null=True)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()
    seat_number = models.CharField(max_length=50)
