"""lufthansa_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from lufthansa_backend.common.permissions import get_user_permissions

import ticket.urls as ticket_urls
from lufthansa_backend.auth.model_views.views import LBTokenObtainPairView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/knock-knock/', LBTokenObtainPairView.as_view(), name='knock-knock'),
    path('auth/user-permissions/', get_user_permissions, name='user-permissions'),

]

urlpatterns += ticket_urls.urlpatterns
