from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.permissions import BasePermission, SAFE_METHODS

from lufthansa_backend.cons import GROUP_ADMINISTRATOR, GROUP_USER_PERSON, DATA


def generic_has_permission(self, request, view, group_name):
    return group_name in list(request.user.groups.values_list('name', flat=True))


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class IsAdministrator(BasePermission):
    def has_permission(self, request, view):
        return generic_has_permission(self, request, view, GROUP_ADMINISTRATOR)


class IsSalesPerson(BasePermission):
    def has_permission(self, request, view):
        return generic_has_permission(self, request, view, GROUP_USER_PERSON)


@api_view(['GET'])
def get_user_permissions(request):
    user = request.user
    groups = list(user.groups.all().values_list('name', flat=True))
    if user.is_superuser:
        groups.append('SUPERUSER')
    result = {
        'permissions': list(user.get_all_permissions()),
        'groups': groups
    }
    return Response({DATA: result}, status=status.HTTP_200_OK)


def get_shops(user):
    if user.shops:
        return user.shops.first.id
    return []


@api_view(['GET'])
def get_user_shops(request):
    user = User.objects.filter(id=request.user.id)
    user_agent = user.first()
    if user.exists() and user_agent.is_authenticated:
        return Response({'shops': ShopReadSerializer(user_agent.shops.all(), many=True).data})
    return Response({'error': 'Ky përdorues nuk ekziston'})
