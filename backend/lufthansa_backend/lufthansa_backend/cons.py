DATA = 'data'
ERRORS = 'errors'
VALID = 'valid'
MESSAGE = 'message'
FILTER_PREFIX = 'flt_'
ERROR_TYPE = 'type'

VALIDATION_ERROR = 'ValidationError'
HTTP_404 = 'Http404'
HTTP_403 = 'Http403'
INTEGRITY_ERROR = 'IntegrityError'
OTHER = 'Other'
FIELD_ERROR = 'FieldError'
INVALID_DATA = 'InvalidData'
UNAUTHORIZED = 'Unauthorized'
EMAIL_ERROR = 'EmailError'

GROUP_ADMINISTRATOR = 'Admin'
GROUP_USER_PERSON = 'User'

def get_validation_error_message(error_data):
    try:
        response_message = ''
        if isinstance(error_data, dict):
            for key, value in error_data.items():
                for item in value:
                    if 'message' in item:
                        response_message += '{}: {} '.format(key, item['message'])
                    else:
                        if isinstance(item, dict):
                            for unit_key, unit_value in item.items():
                                for arr_item in unit_value:
                                    response_message += '{}: {} '.format(unit_key, arr_item['message'])
                        elif isinstance(item, str):
                            for item_key, item_val in value.items():
                                for arr_item in item_val:
                                    response_message += '{}: {} '.format(item_key, arr_item['message'])
        elif isinstance(error_data, list):
            for arr_item in error_data:
                response_message += '{} '.format(arr_item['message'])
    except:
        response_message = 'Error {}'.format(error_data)
    return response_message
