from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from six import text_type


class LBTokenObtainPairSerializer(TokenObtainPairSerializer):
    def update(self, instance, validated_data):
        return super(LBTokenObtainPairSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        return super(LBTokenObtainPairSerializer, self).create(validated_data)

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        data = super(LBTokenObtainPairSerializer, self).validate(attrs)
        groups = list(self.user.groups.all().values_list('name', flat=True))
        refresh = self.get_token(self.user)
        data['refresh'] = text_type(refresh)
        data['access'] = text_type(refresh.access_token)
        data['groups'] = groups
        data['permissions'] = list(self.user.get_all_permissions())
        data['username'] = self.user.username

        # data['username'] = self.user.username
        # data['shop'] = get_shops(self.user)
        return data
