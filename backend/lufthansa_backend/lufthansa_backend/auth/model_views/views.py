from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import TokenError, InvalidToken
from rest_framework_simplejwt.views import TokenObtainPairView

from lufthansa_backend.auth.serializers.auth_serializers import LBTokenObtainPairSerializer
from lufthansa_backend.cons import ERROR_TYPE, VALIDATION_ERROR, ERRORS


class LBTokenObtainPairView(TokenObtainPairView):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = LBTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])
        except ValidationError as e:
            return Response(
                {ERROR_TYPE: VALIDATION_ERROR, ERRORS: e.get_full_details(), MESSAGE: 'Kredenciale të gabuara!'},
                status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({ERROR_TYPE: OTHER, ERRORS: '{}'.format(e), MESSAGE: 'Kredenciale të gabuara!'},
                            status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)
