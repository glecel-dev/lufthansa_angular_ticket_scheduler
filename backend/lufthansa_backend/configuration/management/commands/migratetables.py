from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from django.db import connection
from django_common.auth_backends import User


class Command(BaseCommand):
    help = 'Migrates Tables for the NewUsers and Groups'

    def handle(self, *args, **kwargs):
        with connection.cursor() as cursor:
            cursor.execute(open("tables_schema.sql", "r").read())
        admin = Group.objects.get(name='Admin')
        user = Group.objects.get(name='User')

        user1 = User.objects.create_user("admin", password='admin')
        user1.is_superuser = True
        user1.is_staff = True
        user1.save()
        user1.groups.add(admin)
        user1.save()

        user2 = User.objects.create_user("user", password='user')
        user2.is_superuser = False
        user2.is_staff = True
        user2.save()
        user2.groups.add(user)
        user2.save()

        print("Tables created successfully")
